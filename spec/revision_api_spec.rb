require './revision_api'
require 'rack/test'

describe 'RevisionApi' do
  include Rack::Test::Methods

  def app
    RevisionApi.new
  end

  it 'returns revisions' do
    get '/revisions'
    expect(last_response.status).to eq(200)
  end

  it 'returns revisions with pagination' do
    get '/revisions?page_no=2'
    expect(last_response.status).to eq(200)
  end

  it 'returns revisions with a given id' do
    get '/revisions/786433'
    expect(last_response.status).to eq(200)
  end

  it 'returns HTTP 500 for any dummy id' do
    get '/revisions/some_dummy'
    expect(last_response.status).to eq(500)
  end
end
