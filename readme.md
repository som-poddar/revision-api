# Revision App
This is an app built with `sinatra` as per the interview assignment.To run the app, execute (will use port :8080). All **assumptions** are highlighted within each routes.


```sh
bundle exec unicorn
```

The app has following routes/end-points ...

##revisions
```sh
/revisions
```
will page through all revisions (20 at a time).
#####Notes
* The route takes an optional parameter `page_no` for paginations
* **assumption:** ` graph_type=node` are only shown on the UI assuming they are the one only relevant for this excercise.
* **assumption:** A new index have been built on `graph_type` on *changesets2* table.

##revision
```sh
/revisions/:id
```
will show a particular revision
#####Notes
* The route doesn't take any parameter(s)
* If no id matches the param, return `http 500` 

##decision
```sh
/revisions/:id/:decision
```
will `accept` or `reject` any revisions.
#####Notes
* **assumption** : I have assumed 'needs_moderation' stores review decision in DB, 
flag is set to 1 if 'denied', else 0 
