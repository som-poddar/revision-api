if %w(staging production).include?(ENV['RUBY_ENV'])
  worker_processes 2
  timeout 90
else
  worker_processes 1
  timeout 9999
  listen 8080
end
