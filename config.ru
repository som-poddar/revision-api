ENV['RACK_ENV'] = ENV['RUBY_ENV'] || 'development'
ENV['APP_ROOT'] = Dir.pwd

require './revision_api'
require 'rack'
require 'rubygems'
require 'bundler'

Bundler.require
use Rack::Deflater
run RevisionApi
