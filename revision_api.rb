require 'sinatra'
require 'sinatra/activerecord'
require 'will_paginate'
require 'will_paginate/active_record'

# the following properties set the table name and primary key
class Revision < ActiveRecord::Base
  self.table_name = 'changesets2'
  self.primary_key = 'ID'
end

# end-points for revision app
class RevisionApi < Sinatra::Base
  get '/revisions' do
    @page = params[:page_no] || 1
    @next_page = @page.to_i + 1
    @previous_page = @page.to_i - 1
    @revisions = Revision.where(graph_type: 'node')
                 .paginate(page: @page, per_page: 20)
                 .order('priority')
    erb :revisions
  end

  get '/revisions/:id' do
    # TODO: Add any check for format/sanity of ID
    # It can potentially cut-down load on DB

    @revision = Revision.find_by_id(params[:id])
    erb :revision
  end

  get '/revisions/:id/:decision' do
    # NOTE: I have assumed 'needs_moderation' stores review decision
    # flag is set to 1 if 'denied', else 0

    @revision = Revision.find_by_id(params[:id])
    update_val = params[:decision] == 'approve' ? false : true
    redirect '/revisions' if @revision.update(needs_moderation: update_val)
  end
end
